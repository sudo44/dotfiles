#!/bin/sh

if type brew > /dev/null 2>&1; then
  echo 'OK! already installed brew.'
else
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Make sure using latest Homebrew
echo 'start brew update...'
brew update

# Update already-installed formula
echo 'start brew upgrade...'
brew upgrade

# Formula
brew install vim git mas
# brew tap sanemat/font
# brew install ricty

# .dmg apps

brew cask install clipy
brew cask install visual-studio-code
brew cask install google-chrome
brew cask install google-japanese-ime
brew cask install cmd-eikana

# appstore app
mas install 409183694  # Keynote
mas install 409201541  # Pages
mas install 409203825  # Numbers

# Remove outdated versions
brew cleanup
