#!/bin/sh

if [ ! -L ~/server ]; then
	ln -s ~/Google\ ドライブ/server ~/server
fi

if [ ! -d ~/.ssh ]; then
	mkdir ~/.ssh
fi

if [ ! -L ~/.ssh/config ]; then
	ln -s ~/Google\ ドライブ/server/ssh/config ~/.ssh/config
fi

if [ ! -L ~/.key ]; then
	ln -s ~/Google\ ドライブ/server/key ~/.key
fi



if [ -e ~/.oh-my-zsh/custom/custom-aliases.zsh ]; then
	rm ~/.oh-my-zsh/custom/custom-aliases.zsh
fi
if [ ! -L ~/.oh-my-zsh/custom/custom-aliases.zsh ]; then
	ln -s ${PWD}/custom-aliases.zsh ~/.oh-my-zsh/custom/custom-aliases.zsh
fi
