#!/bin/sh

if type brew > /dev/null 2>&1; then
  echo 'OK! already installed brew.'
else
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Make sure using latest Homebrew
echo 'start brew update...'
brew update

# Update already-installed formula
echo 'start brew upgrade...'
brew upgrade

# Add Repository
# tap homebrew/versions
# tap homebrew/binary

# For cask
# brew tap caskroom/cask
# brew install brew-cask

# Formula
brew install vim curl git ag mysql jq watch awscli mas
brew install zsh --without-etcdir
brew tap sanemat/font
brew install ricty

# .dmg apps

brew cask install iterm2
brew cask install virtualbox
brew cask install vagrant
brew cask install dropbox
brew cask install firefox
brew cask install clipy
brew cask install deltawalker
brew cask install appcleaner
brew cask install visual-studio-code
brew cask install google-chrome
brew cask install google-backup-and-sync
brew cask install google-japanese-ime
brew cask install kindle
brew cask install sourcetree
brew cask install karabiner-elements
brew cask install forticlient
brew cask install postman

echo "install '1password 7'(need license) [Y/n]?"
read ANSWER
if [[ $ANSWER =~ ^[Yy]$ ]]
then
    brew cask install 1password
else
  echo "skip 1password 7."
fi

# appstore app
mas install 539883307  # line
echo "install '1password 6'(free license) [Y/n]?"
read ANSWER
if [[ $ANSWER =~ ^[Yy]$ ]]
then
    mas install 443987910  # 1password 6
else
  echo "skip 1password 6."
fi
mas install 409183694  # Keynote
mas install 409201541  # Pages
mas install 409203825  # Numbers


# Remove outdated versions
brew cleanup
