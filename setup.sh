#!/bin/sh

${PWD}/brewfile.sh

# gitconfig
if [ -e ~/.gitconfig ]; then
	rm ~/.gitconfig
fi
if [ ! -L ~/.gitconfig ]; then
	ln -s ${PWD}/gitconfig ~/.gitconfig
fi
if [ ! -L ~/.gitignore_global ]; then
	ln -s ${PWD}/gitignore_global ~/.gitignore_global
fi


#zsh
grep '/usr/local/bin/zsh' /etc/shells 1> /dev/null 2>/dev/null
if [ $? -eq 0 ]; then
  echo "OK! already added /usr/local/bin/zsh to /etc/shells"
else
	sudo sh -c "echo '/usr/local/bin/zsh' >> /etc/shells"
fi
if [ $SHELL = '/usr/local/bin/zsh' ]; then
	echo "OK! already changed $SHELL to zsh"
else
	chsh -s "/usr/local/bin/zsh"
fi
if [ ! -d ~/.oh-my-zsh ]; then
	sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
fi

# # なぜかうまく動かない。。
# sed -i '' -E 's/ZSH_THEME="[^"]+"/ZSH_THEME="amuse"/g' ~/.zshrc
# sed -i '' -E 's/# export PATH=/export PATH=/g' ~/.zshrc
# source ~/.zshrc

#ricty
ls ~/Library/Fonts/Ricty* 1> /dev/null 2>/dev/null
if [ $? -eq 0 ]; then
	echo "OK! already installed ricty"
else
	cp -f /usr/local/opt/ricty/share/fonts/Ricty*.ttf ~/Library/Fonts/
fi
